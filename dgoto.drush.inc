<?php
/**
 * @file
 * This Drush (http://drupal.org/project/drush) extension uses the http://dgo.to
 * service to find  user ids from user names on Drupal.org or the various
 * #drupal IRC channels. See http://dgo.to for more information about this
 * service. This is just a (command line) cURL wrapper, which retrieves the
 * Location: header.
 *
 * See drush help for more info.
 *
 * @author Josh The Geek (http://drupal.org/user/926382)
 */

/**
 * Implementation of hook_drush_help().
 */
function dgoto_drush_help($section) {
  switch ($section) {
    case 'drush:dgoto-user':
      return dt('Get a Drupal.org user ID from a user name.');
      break;
    case 'drush:dgoto-irc':
      return dt('Get a Drupal.org user ID from a user name in the various #drupal IRC channels.');
      break;
  }
}

/**
 * Implementation of hook_drush_command().
 */
function dgoto_drush_command() {
  $items = array();

  $items['dgoto-user'] = array(
    'description' => 'Get a Drupal.org user ID from a user name, using dgo.to.',
    'arguments' => array(
      'user name' => 'The user name to retrieve the user ID of.',
    ),
    'examples' => array(
      'drush dgoto-user Dries' => 'Will return Dries\'s user ID, 1.',
      'drush dgoto-user Josh The Geek' => 'This works with spaces, too. This will output 926382.',
    ),
    'aliases' => array('duser'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap is needed by this command.
  );
  $items['dgoto-irc'] = array(
    'description' => 'Get a Drupal.org user ID from a user name used in the #drupal IRC channels, using dgo.to.',
    'arguments' => array(
      'user name' => 'The IRC user name to retrieve the user ID of. Note that this may not work if the user has not entered a IRC nick in their profile.',
    ),
    'examples' => array(
      'drush dgoto-irc emspace' => 'Gives you sime\'s user id, because he has a different IRC nick from his user name.',
    ),
    'aliases' => array('dgoto-nick', 'dirc', 'dnick'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap is needed by this command
  );
  return $item;
}

/**
 * Implementation of drush_hook_COMMAND().
 */
function drush_dgoto_dgoto_user($user) {
  // Note: I don't call drush_escapeshellarg() here because drush_shell_exec
  // does it for me.
  $user = trim(urlencode($user));
  $url = 'http://dgo.to/@' . $user;
  # Explain:    -I = Return headers   grab Location header     Leave just the url            get the user id
  $command = "curl %s -I 2> /dev/null | grep Location: | cut -d : -f 2,3 | cut -d ' ' -f 2 | cut -d '/' -f 5";
  if (!drush_shell_exec($command, $url)) {
    return drush_set_error('DRUSH_cURL_ERROR', 'curl returned an error. Check that curl is installed and in your path.');
  }
  drush_print(dt('@name\'s user id is: !uid', array(
    '@name' => $user,
    '!uid' => drush_shell_exec_output(),
  )));
  return true;
}

/**
 * Implementation of drush_hook_COMMAND().
 */
function drush_dgoto_dgoto_irc($user) {
  // Note: I don't call drush_escapeshellarg() here because drush_shell_exec
  // does it for me.
  $user = trim(urlencode($user));
  $url = 'http://dgo.to/irc/' . $user;
  # Explain:
  $command = "curl %s -I 2> /dev/null | grep Location: | cut -d : -f 2,3 | cut -d ' ' -f 2 | cut -d '/' -f 5";
  if (!drush_shell_exec($command, $url)) {
    return drush_set_error('DRUSH_cURL_ERROR', 'curl returned an error. Check that curl is installed and in your path. Note that the IRC nick lookup only works when the user has entered their IRC nick into their profile');
  }
  drush_print(dt('The IRC nick @name\'s user id is: !uid', array(
    '@name' => $user,
    '!uid' => drush_shell_exec_output(),
  )));
  return true;
}

